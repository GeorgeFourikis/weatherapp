const axios = require('axios')
const moment = require('moment')
const config = require('./config/config.json')

const rawDate = new Date("2018-03-04")
const showDate = moment(rawDate).format('YYYY-MMMM-DD')
const desiredDate = moment(rawDate).format('YYYYMMDD')
const url = `http://api.wunderground.com/api/${config.myApiKey}/history_${desiredDate}/q/NY/New_York.json `

axios.get(url)
    .then((data)=>{
        console.log(`Here are the following data for New York, date: ${showDate}`)
        console.log(`The max Humidity was: ${data.data.history.dailysummary[0].maxhumidity}%`)
        console.log(`The min Temperature was: ${data.data.history.dailysummary[0].mintempm} degrees C`)
        console.log(`The max Temperature was: ${data.data.history.dailysummary[0].maxtempm} degrees C`)
        console.log(`The Precipitation was: ${data.data.history.dailysummary[0].precipm} mm`)
    })
    .catch((err)=>{
        console.log(err)
    })
