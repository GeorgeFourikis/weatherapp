This small application is in response to the challenge.
===

The reason i used Node js is because i am more familiar with Node since i have professional experience with it.
The packages used in this application are: Axios (for the requests), moment (to format the date if needed) and 
nothing more is needed for now.The structure is simple with a main file (app.js) and a config.json which carries the 
API key and since it is a personal and dedicated key i felt i should have it in a separate file to ignore it from GIT.


On the task, the AXIOS library provides the method GET, which gets a URL as an input and it returns (using a promise) the 
data, if an error exists, the error will be logged.
After we run the application, it should give us a message in the console:

```
Here are the following data for New York, date: 2018-March-04
The max Humidity was: 64%
The min Temperature was: 1 degrees C
The max Temperature was: 7 degrees C
The Precipitation was: 0.00 mm
```